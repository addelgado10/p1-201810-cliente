Alan David Delgado Prieto 	201613997
Juan Sebastian Martinez Ni�o	201615546

Estimacion de complejidad

1a. O(n*log(n)) porque ordena la lista de servicios segun su hora de fecha inical utilizando merge, y se agregan a la cola los que se ecnuentren en el rOOango escrito por el usuario

2a. O(n) recorre toda la lista y guarda al mayor

3a. O(n) recorre toda la lista una vez

4a. O(n*log(n)) ordena la lista con merge segun la distancia recorrida y las organiza segun los requerimento

2a. O(n^2) recorre la lista de servicios y crea un contador para cada compa�ia, pero revisa en una lista temporal si la compa�ia revisada ya tiene contador.

2b. O(n) recorre toda la lista buscado el taxi

3b. O(n) recorre toda la lista 

4b. O(n*log(n)) ordena la lista segun su zona y su fecha y hora

1c. O(n) genera el data building y lo guarda en una liked list

2c. O(n*log(n)) copiar la lista a una lista auxiliar, en la lista auxiliar coger el primer servicio revisar su compa�ia y desconectar de la lista todos los servicios con esa compa�ia, por lo tanto la lista auxiliar se va reduciendo generando una busqueda logaritmica.

3c. O(n) busca el taxi en la lista de servicios

4c. O(n) recorre la lista y guarda los sevicios en una cola y despues los resume