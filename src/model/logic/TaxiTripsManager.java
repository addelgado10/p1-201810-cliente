package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.LinkedListt;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FinancialData;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

import javax.json.stream.*;
import javax.json.stream.JsonParser.Event;
import javax.jws.soap.SOAPBinding.ParameterStyle;

import org.glassfish.json.JsonParserImpl;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import com.sun.java.swing.plaf.windows.WindowsTreeUI.CollapsedIcon;
import com.sun.org.apache.regexp.internal.ReaderCharacterIterator;










import org.glassfish.json.*;

import sun.awt.image.ImageWatched.Link;
import sun.launcher.resources.launcher;


public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";



	private LinkedListt<Servicio> linkedListtServicios;
	@Override //1C
	public boolean cargarSistema(String direccionJson)  
	{

		boolean resp = true;

		Gson gson = new  Gson();
		linkedListtServicios = new LinkedListt();

		try {
			Servicio[] listt= gson.fromJson(new FileReader(direccionJson), Servicio[].class);
			for(int i = 0; i < listt.length;i++)
			{
				linkedListtServicios.addFirst(listt[i]);
				//temporall.add(listt[i]);

			}
			System.out.println(listt.length);

		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}

	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	/*
	 * Orden cronologico de los servicios realizados dentro del rango dado por par�metro
	 * El inicio y fin del servicio debe estar dentro del rango
	 * 1. Verificar que este dontro del rango dado por par�metro
	 * 2.1 a�adir como cabeza si la lista esta vacia
	 * 2.2 Encontrar la posicion o rango en donde se debe a�adir 
	 * 3.1 A�adir a lo ultimo si es mayor al �ltimo dato
	 * 3.2 A�adir en la cabeza si es menor al primero
	 * 3.3 Definir si se debe a�adir en la primera mitad de la lista o en la segunda
	 * 4.1 Organizar en la primera mitad (no incluye el dato medio)
	 * 4.2 Organizar en la segunda mitad (incluye el dato medio)
	 * 5.1 Comparar y a�adir antes del dato que sea mayor
	 */
	{Queue<Servicio> resp = new Queue<>();
	//Lista de menor a mayor
	LinkedListt<Servicio> list=new LinkedListt<>();
	//Servicio a a�adir
	Servicio serv=null;
	//Servicio temporal
	Servicio temp=null;
	//Rango del servicio a a�adir
	RangoFechaHora rang1=null;
	//Rango del servicio temporal
	RangoFechaHora rang2=null;

	for (int k =1;k <= linkedListtServicios.size();k++){
		serv=linkedListtServicios.getIndex(k);
		//1.
		if(rango.estaDentro(serv.darHorario())){
			//2.1
			if(list.isEmpty()){
				list.addFirst(serv);
			}
			//2.2
			else{
				rang1=serv.darHorario();
				temp=list.getIndex(list.size());
				rang2=temp.darHorario();
				//3.1
				if(rang1.getFechaInicial().compareTo(rang2.getFechaInicial())>=0){
					list.addLast(serv);
				}
				else{
					//3.2
					temp=(Servicio) list.first();
					rang2=temp.darHorario();
					if(rang1.getFechaInicial().compareTo(rang2.getFechaInicial())<=0){
						list.addFirst(serv);
					}
					else{
						//3.3
						temp=list.getIndex(Math.round(list.size()/2));
						rang2=temp.darHorario();
						if(rang1.getFechaInicial().compareTo(rang2.getFechaInicial())<0){
							//4.1
							for(int i =1;i<Math.round(list.size()/2);i++){
								//5.1
								temp=list.getIndex(i);
								rang2=temp.darHorario();
								if(rang1.getFechaInicial().compareTo(rang2.getFechaInicial())>=0 && rang1.getHoraInicio().compareTo(rang2.getHoraInicio())>=0){
									//A�adir antes del que es mayor
									list.addIndex(i-1, serv);
									break;
								}
							}
						}
						else{
							//4.2
							for(int i = Math.round(list.size()/2);i<list.size();i++){
								//5.1
								temp=list.getIndex(i);
								rang2=temp.darHorario();
								if(rang1.getFechaInicial().compareTo(rang2.getFechaInicial())>=0 && rang1.getHoraInicio().compareTo(rang2.getHoraInicio())>=0){
									//A�adir antes del que es mayor
									list.addIndex(i-1, serv);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	//A�adir a la cola
	for(int i =1; i <=list.size();i++){
		resp.enqueue(list.getIndex(i));
	}
	//Retornar cola
	return resp;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		/**
		 *�Usar el primer metodo? 
		 */

		/*
		 * Buscar taxi por compa�ia
		 * Taxi con mas servicios iniciados en el rango dado por par�metro
		 * 1.1	Comparar los servicios que coincidan con la compa�ia espesificada
		 * 1.2	Contar cuantas veces el taxi cumple con las condiciones
		 * 2.	Encontrar cual es el taxi que m�s veces aparece en la lista
		 */
		Taxi resp=null;
		//Cola con los servicios que coinciden
		Queue<Servicio> cola = (Queue<Servicio>) darServiciosEnPeriodo(rango);
		//Comparador
		int comp = 0;
		//Lista con los taxisId
		LinkedListt<String> taxiId=new LinkedListt<>();
		//Lista con lacantidad de servicios del taxi
		LinkedListt<Integer> numServicios=new LinkedListt<>();

		while(!cola. isEmpty()){
			if(!cola.first().getCompany().equals(company)){
				if(taxiId.isEmpty()){
					taxiId.addFirst(cola.dequeue());
					numServicios.addFirst(1);
				}
				else{
					for(int i=0; i <=taxiId.size()-1;i++){
						if(cola.first().getTaxiId().equals(taxiId.getIndex(i))){
							cola.dequeue();
							numServicios.addIndex(i, numServicios.getIndex(i)+1);
						}
					}
				}
			}
			else{
				cola.dequeue();
			}
		}

		//Hallar el taxixon mas servicios
		for(int i =1; i<=numServicios.size(); i++){
			if(comp<=numServicios.getIndex(i)){
				comp=numServicios.getIndex(i);
				resp=new Taxi(taxiId.getIndex(i), company);
			}
		}


		return resp;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{

		/*
		 * Buscar Taxi a partir de su identificador
		 * Dentro del rango de fecha y hora dado por par�metro
		 * 1.	Definir servicios prestados en el rango dado por par�ametro
		 * 2.	Identificar taxi con la ID ingresada por par�metro
		 * 3.	Crear el InfoTaxi rango
		 */

		//1.
		Queue<Servicio> cola=(Queue<Servicio>) darServiciosEnPeriodo(rango);;

		InfoTaxiRango resp = new InfoTaxiRango();
		LinkedListt<Servicio> servicios = new LinkedListt<>();
		//2.
		while(cola.isEmpty()){
			if(cola.first().getTaxiId().equals(id)){
				//3.
				resp.setCompany(cola.first().getCompany());
				resp.setDistanciaTotalRecorrida(cola.first().getTripMiles());
				resp.setIdTaxi(cola.first().getTaxiId());
				resp.setPlataGanada(cola.first().darTotal());
				resp.setRango(cola.first().darHorario());
				resp.setTiempoTotal(cola.first().getTripSeconds());
				servicios.addFirst(cola.first());
				break;
			}
			cola.dequeue();
		}

		//3.
		if(!cola.isEmpty()){
			while(!cola.isEmpty()){
				if(cola.first().getTaxiId().equals(id)){
					servicios.addFirst(cola.first());
				}
				cola.dequeue();
			}
		}
		resp.setServiciosPrestadosEnRango(servicios);

		return resp;
	}

	@Override //4A
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		/*
		 * LinkedListt de rangos
		 * 	Los rangos van de la forma (En millas):
		 * 		[0,1)
		 * 		[1,2)
		 * 		...
		 * 
		 * 1.1	Identificar los taxis que cumplan con las condiciones
		 * 			Pertenecer a una compa�ia
		 * 			Estar en la fecha espesificada
		 * 			Estar dentro del rango de horas
		 * 1.2	Definir el valor del viaje mas largo
		 * 2.	Crear las listas con los servicios pertenecientes a cada rango
		 * 3.1	Crear los "RangoDistancia"
		 * 3.2	Agregar los rango distancia a la lista respuesta
		 */

		//Contador de el viaje con mas millas
		int maxMillas=0;
		//Lista con los rangos
		LinkedListt<RangoDistancia> resp = new LinkedListt<>();
		//Lista con los servicios que cumplen las condidiciones
		LinkedListt<Servicio> servicios = new LinkedListt<>();
		//Arreglo de listas conservicios pertenecientes a cada rango
		LinkedListt<Servicio> [] rangos=null;
		//Arreglo de "RangoDistancia"
		RangoDistancia[] rD = null;


		//1.1
		for (int k =1; k <= linkedListtServicios.size(); k++){
			if(!linkedListtServicios.getIndex(k).getCompany().isEmpty() && linkedListtServicios.getIndex(k).darHorario().getFechaFinal().equals(linkedListtServicios.getIndex(k).darHorario().getFechaFinal()) && linkedListtServicios.getIndex(k).darHorario().getHoraFinal().compareTo(horaFinal)<=0 && linkedListtServicios.getIndex(k).darHorario().getHoraInicio().compareTo(horaInicial)>=0){
				servicios.addFirst(linkedListtServicios.getIndex(k));
				//1.2
				if(maxMillas<=linkedListtServicios.getIndex(k).getTripMiles()){
					maxMillas=(int) Math.ceil(linkedListtServicios.getIndex(k).getTripMiles());
				}
			}
		}
		//2.
		rangos= new LinkedListt[maxMillas];
		for(int i = 1; i <=maxMillas; i++){
			if(servicios.getIndex(i).getTripMiles()>=i && servicios.getIndex(i).getTripMiles()<i+1 ){
				rangos[i].addFirst(servicios.getIndex(i));
				servicios.removeIndex(i);
			}
		}

		//3.1
		rD=new RangoDistancia[maxMillas];
		//3.2
		for(int i = 1; i <=maxMillas; i++){
			rD[i]=new RangoDistancia();
			rD[i].setLimineInferior(i);
			rD[i].setLimiteSuperior(i+1);
			rD[i].setServiciosEnRango(rangos[i]);
			resp.addFirst(rD[i]);
		}

		return null;
	}


	@Override //1B
	public LinkedListt<Compania> darCompaniasTaxisInscritos() 
	{
		LinkedListt<Taxi> linkedListtTaxis= new LinkedListt<>();
		for(int i = 0;i < linkedListtServicios.size();i++)
		{
			Servicio temporal = linkedListtServicios.getIndex(i);
			if(temporal.getCompany()!=null)
			{
				Taxi nuevo = temporal.darTaxi();
				linkedListtTaxis.addFirst(nuevo);
			}			
		}
		ArrayList<Compania> nuav = new ArrayList<>();
		LinkedListt<Compania> compania = new LinkedListt<Compania>();
		int j = 0;
		int numTaxis = 0;
		boolean repetido = false;
		while(linkedListtTaxis.size()>0)
		{
			LinkedListt<Taxi> taxii = new LinkedListt<Taxi>();
			Compania nueva = new Compania();
			Taxi temp = (Taxi)linkedListtTaxis.first();
			nueva.setNombre(temp.getCompany());
			j = 0;
			while(j< linkedListtTaxis.size())
			{
				Taxi temp2 = (Taxi)linkedListtTaxis.getIndex(j);
				if(temp.getCompany().equals(temp2.getCompany()))
				{
					repetido = false;
					linkedListtTaxis.removeIndex(j);
					j--;
					for(int k = 0;k < taxii.size();k++)
					{
						Taxi temp3 = (Taxi)taxii.getIndex(k);
						if(temp3.compareTo(temp2) == 0)
						{
							repetido = true;
						}
					}
					if(!repetido)
					{
						taxii.addFirst(temp2);
						numTaxis++;
					}
				}
				j++;
			}
			nueva.setTaxisInscritos(taxii);
			compania.addLast(nueva);
		}
		for(int i = 1; i < compania.size();i++)
		{
			nuav.add(compania.getIndex(i));
		}
		int salto, i;
		boolean cambios;
		for(salto=compania.size()/2; salto!=0; salto/=2){
			cambios=true;
			while(cambios){ // Mientras se intercambie alg�n elemento
				cambios=false;
				for(i=salto; i< nuav.size(); i++){ // se da una pasada{
					Compania temp = nuav.get(i-salto);
					Compania temp2 = nuav.get(i);
					if(temp.compareTo(temp2)>=0 ){ // y si est�n desordenados

						nuav.remove(i);
						nuav.add(i, temp);
						nuav.remove(i-salto);
						nuav.add(i-salto, temp2);
						cambios=true; // y se marca como cambio.
					}
				}
			}
		}
		System.out.println("El numero total de Taxis que prestan servicio a una sola compa�ia es de: "+numTaxis);
		System.out.println("El numero total de Companias con al menos un taxi Inscrito :"+(compania.size()));
		compania = new LinkedListt<>();
		for(int k = nuav.size()-1;k>=0;k--)
		{
			Compania ayuda = nuav.get(k);
			compania.addFirst(ayuda);
		}
		for(int l = 1;l<compania.size();l++)
		{
			Compania resp =(Compania)compania.getIndex(l);
			System.out.println(resp.getNombre()+" "+resp.getTaxisInscritos().size());
		}
		return compania;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		LinkedListt vector = new LinkedListt<>();
		String[] companyy = nomCompania.split("-");
		String nomCompani = "";
		double dinero = 0;
		double comparar = 0;
		Taxi resp = null;
		for(int i = 0;i <companyy.length-1;i++)
		{
			nomCompani += companyy[i] + " ";
		}
		nomCompani += companyy[companyy.length-1];
		for(int i = 0;i < linkedListtServicios.size();i++)
		{
			Servicio temporal = linkedListtServicios.getIndex(i);
			if(temporal.getCompany()!=null)
			{
				RangoFechaHora temp = temporal.darHorario();
				if(rango.estaDentro(temp) && temporal.getCompany().equals(nomCompani))
				{
					vector.addFirst(temporal);
				}				
			}			
		}
		for(int i =0;i<vector.size();i++)
		{
			dinero = 0;
			Servicio actual = (Servicio) vector.getIndex(i);
			for(int j= 0;j < vector.size();j++)
			{
				Servicio siguiente = (Servicio) vector.getIndex(j);
				if(siguiente.darTaxi().getTaxiId().equals(actual.darTaxi().getTaxiId()))
				{
					dinero+= siguiente.darTotal();
				}
			}
			if(dinero>comparar)
			{
				comparar=dinero;
				resp = actual.darTaxi();
			}

		}
		System.out.println(comparar);
		System.out.println(vector.size());
		System.out.println(resp.getTaxiId());
		System.out.println(resp.getCompany());
		return resp;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		double valor1 = 0;
		double valor2 = 0;
		double valor3 = 0;
		LinkedListt vector1 = new LinkedListt<>();
		LinkedListt vector2 = new LinkedListt<>();
		LinkedListt vector3 = new LinkedListt<>();
		for(int i = 0;i < linkedListtServicios.size();i++)
		{
			Servicio temporal = linkedListtServicios.getIndex(i);
			RangoFechaHora temp = temporal.darHorario();
			if(rango.estaDentro(temp) && temporal.darZonaInicio()!=null && temporal.darZonaFinal()!=null)
			{	
				if(temporal.darZonaInicio().equals(idZona) && !temporal.darZonaFinal().equals(idZona))
				{
					vector1.addFirst(temporal);	
					valor1 += temporal.darTotal();
				}

				else if(!temporal.darZonaInicio().equals(idZona) && temporal.darZonaFinal().equals(idZona))
				{
					vector2.addFirst(temporal);
					valor2 += temporal.darTotal();
				}
				else if(temporal.darZonaInicio().equals(idZona) && temporal.darZonaFinal().equals(idZona))
				{
					vector3.addFirst(temporal);	
					valor3 += temporal.darTotal();
				}
			}							
		}
		ServiciosValorPagado inicio = new ServiciosValorPagado();
		inicio.setValorAcumulado(valor1);
		inicio.setServiciosAsociados(vector1);
		ServiciosValorPagado finall = new ServiciosValorPagado();
		inicio.setValorAcumulado(valor2);
		inicio.setServiciosAsociados(vector2);
		ServiciosValorPagado ambos = new ServiciosValorPagado();
		inicio.setValorAcumulado(valor3);
		inicio.setServiciosAsociados(vector3);
		ServiciosValorPagado[] resp = new ServiciosValorPagado[3];
		resp[0] = inicio;
		resp[1] = finall;
		resp[2] = ambos;

		System.out.println("El n�mero total de servicios que se recogieron en la zona de consulta y terminaron en otra zona es: " + vector1.size());
		System.out.println("Y el valo total pagado por los usuarios es de: " + valor1);
		System.out.println("El n�mero total de servicios que se recogieron en otra zona y terminaron en la zona de consulta es: " + vector2.size());
		System.out.println("Y el valo total pagado por los usuarios es de: " + valor2);
		System.out.println("El n�mero total de servicios que se recogieron en la zona de consulta y terminaron en la zona de consulta es: " + vector3.size());
		System.out.println("Y el valo total pagado por los usuarios es de: " + valor3);
		return resp;
	}

	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		LinkedListt<ZonaServicios> resp = new LinkedListt<>();
		LinkedListt<Servicio> aux = new LinkedListt<>();
		for(int i = 0 ; i< linkedListtServicios.size();i++)
		{	
			Servicio temporal = linkedListtServicios.getIndex(i);
			if(temporal.darZonaInicio()!=null && rango.estaDentro(temporal.darHorario()))
				aux.addFirst(temporal);
		}
		int cod = 0;
		int j = 0;
		boolean existe = false;
		SimpleDateFormat dia = new SimpleDateFormat("yyyy-MM-dd");
		while(aux.size() > 0 )
		{	
			existe = false;
			LinkedListt ayuda = new LinkedListt<>();
			ZonaServicios zona = new ZonaServicios();
			zona.setIdZona(String.valueOf(cod));
			j=0;
			while(j<aux.size())
			{
				Servicio temporal = aux.getIndex(j);
				if(temporal.darZonaInicio()!=null && rango.estaDentro(temporal.darHorario()))
				{

					if(temporal.darZonaInicio().equals(Integer.toString(cod)))
					{
						existe = true;
						try {
							Date diainicio = dia.parse(temporal.darHorario().getFechaInicial());
							aux.removeIndex(j);
							j--;
							ayuda.addFirst(diainicio);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				j++;
			}

			cod++;
			if(existe)
			{
				zona.setFechasServicios(ayuda);
				resp.addFirst(zona);
			}

		}
		System.out.println("final"+resp.size());
		return resp;
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		LinkedListt<CompaniaServicios> resp = new LinkedListt<>();
		LinkedListt<Servicio> vector = new LinkedListt<>();
		ArrayList<CompaniaServicios> nuav =  new ArrayList<>();
		for(int i = 0;i < linkedListtServicios.size();i++)
		{
			Servicio temporal = linkedListtServicios.getIndex(i);
			RangoFechaHora temp = temporal.darHorario();
			if(rango.estaDentro(temp) && temporal.getCompany() != null)
			{
				vector.addFirst(temporal);
			}				
		}		
		while(vector.size()>0)
		{
			int j = 0;
			Servicio temporal = vector.getIndex(0);
			LinkedListt<Servicio> ayuda =  new LinkedListt<>();
			CompaniaServicios respuesta = new CompaniaServicios();
			respuesta.setNomCompania(temporal.getCompany());
			while(j<vector.size())
			{
				Servicio temporal2 = vector.getIndex(j);
				if(temporal2.getCompany().equals(temporal.getCompany()))
				{
					vector.removeIndex(j);
					j--;
					ayuda.addFirst(temporal2);
				}
				j++;
			}
			respuesta.setServicios(ayuda);
			resp.addFirst(respuesta);
		}
		for(int i = 1; i < resp.size();i++)
		{
			nuav.add(resp.getIndex(i));
		}
		int salto, i;
		boolean cambios;
		for(salto=resp.size()/2; salto!=0; salto/=2){
			cambios=true;
			while(cambios){ // Mientras se intercambie alg�n elemento
				cambios=false;
				for(i=salto; i< nuav.size(); i++){ // se da una pasada{
					CompaniaServicios temp = nuav.get(i-salto);
					CompaniaServicios temp2 = nuav.get(i);
					if(temp.compareTo(temp2)==-1 ){ // y si est�n desordenados

						nuav.remove(i);
						nuav.add(i, temp);
						nuav.remove(i-salto);
						nuav.add(i-salto, temp2);
						cambios=true; // y se marca como cambio.
					}
				}
			}
		}
		resp = new LinkedListt<>();
		for(int k = 0;k<nuav.size()&&k<n+1;k++)
		{
			CompaniaServicios tempo = nuav.get(k);
			resp.addLast(tempo);
		}
		for(int k = 1;k<resp.size()&&k<n+1;k++)
		{
			CompaniaServicios tempo = resp.getIndex(k);
			System.out.println("La compa�ia: " + tempo.getNomCompania() + " tuvo " + tempo.getServicios().size() + " servicios en el rango especificado");
		}
		return resp;
	}

	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		/*
		 * 	Taxi mas rentable de cada compa�ia
		 * 	El mas rentable es aque cuya razon entre plata ganada y dist. recorrida es mayor (plat. gan. / dist. reco.)
		 * 
		 * 1.	Definir taxis que pertenecen a una compa�ia
		 * 2.1	Buscar el taxi con la mayor razon en cada compa�ia
		 * 2.2	Agregar el taxi a Compa�iaTaxi
		 */

		//Lista Con CompaniaTaxi
		LinkedListt<CompaniaServicios> resp = new LinkedListt<>();
		//Lista de comparadores de la razon 
		LinkedListt<Double> razones = new LinkedListt<>();
		//Lista de nombres de compa�ias
		LinkedListt<String> companias= new LinkedListt<>();
		//Lista de taxisId
		LinkedListt<String> taxiId = new LinkedListt<>();
		//CompaniaTaxi temp
		CompaniaTaxi temp = null;

		for (int i =1; i <=linkedListtServicios.size();i++){
			//Verificar si pertenece a una compa�ia
			if(!linkedListtServicios.getIndex(i).getCompany().isEmpty()){
				//A�adir un nuevo contador para la compa�ia en la posicion j en Companias
				if(companias.isEmpty()){
					companias.addFirst(linkedListtServicios.getIndex(i));
					razones.addFirst(linkedListtServicios.getIndex(i).darTotal()/linkedListtServicios.getIndex(i).getTripMiles());
					taxiId.addFirst(linkedListtServicios.getIndex(i).getTaxiId());
				}
				else{
					for(int j =1; i <=companias.size(); j++){
						//Revisar la posicion de la compa�ia en la lista, para comparar si la razon es mayor
						if(companias.getIndex(j).equals(linkedListtServicios.getIndex(i).getCompany()) && razones.getIndex(j)<(linkedListtServicios.getIndex(i).darTotal()/linkedListtServicios.getIndex(i).getTripMiles())){
							razones.removeIndex(j);
							razones.addIndex(j, (linkedListtServicios.getIndex(i).darTotal()/linkedListtServicios.getIndex(i).getTripMiles()));
							taxiId.removeIndex(j);
							taxiId.addIndex(j, linkedListtServicios.getIndex(i).getTaxiId());
							break;
						}
					}
				}
			}
		}

		//2.2
		for(int i = 1; i <=companias.size(); i++){
			temp=new CompaniaTaxi();
			temp.setNomCompania(companias.getIndex(i));
			temp.setTaxi(new Taxi(taxiId.getIndex(i), companias.getIndex(i)));
			resp.addFirst(temp);
		}
		return resp;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha, Double DistMaxima) 
	{
		/*
		 * 	Servicios de un taxi 
		 * 	Orden cronologico
		 * 		En una fecha espesifica
		 * 		Entre Hora inicial / hora final
		 * 	
		 * 	Ir guardando los servicios hasta exceder la distancia max
		 * 		Si se sobre pasa vaciar y generar servicio resumen
		 * 
		 * Generar Servicio resumen
		 * 		Hora inicial 	--> Menor hora entre los servicios		
		 * 		Hora final 		--> Mayor hora entre los servicios
		 * 		Distancia 		--> SUmatoria de todas las distancias
		 * 		Duracion 		-->	Sumatoria de la duracion de cada servicio
		 * 		Valor total 	--> Sumatoria de todos los valores totales		
		 * 
		 * 
		 *1.1	Determinar servicios del taxi en el rango espesificado
		 *1.2	Guardarlos en una Lista de forma ordenada
		 *2.1	Acomodar los servicios en una cola
		 *2.2	Vaciar si se excede el numero de millas ingresado por par�metro
		 *3.	Generear valores de servicio resumen
		 *4.	Devolver la cola con el(los) servicios
		 */

		//Cola con los servicios del taxi 
		Stack<Servicio> resp = new Stack<>();
		//Contador de las millas max�mas
		double cont = 0;
		//Lista de servicios ordenada (menor a mayor)
		LinkedListt<Servicio> servicios = new LinkedListt<>();
		double distanciaTotal=0;
		int tiempoTotal=0;
		double valorTotal=0;

		//Limite de millas superado
		boolean limSuper=false;


		//1.1 
		for(int i = 1; i <=linkedListtServicios.size(); i++){
			if(linkedListtServicios.getIndex(i).getTaxiId().equals(taxiId) && linkedListtServicios.getIndex(i).darHorario().getFechaInicial().compareTo(fecha)==0 && linkedListtServicios.getIndex(i).darHorario().getFechaFinal().compareTo(fecha)==0 && linkedListtServicios.getIndex(i).darHorario().getHoraInicio().compareTo(horaInicial)>=0 && linkedListtServicios.getIndex(i).darHorario().getHoraFinal().compareTo(horaFinal)<=0){
				//1.2
				//Si la lista esta vacia agregar
				if(servicios.size()==0){
					servicios.addFirst(linkedListtServicios.getIndex(i));
				}
				else{
					//Comparar si se a�ade al final
					if(servicios.getIndex(servicios.size()).darHorario().getHoraInicio().compareTo(linkedListtServicios.getIndex(i).darHorario().getHoraInicio())>=0){
						servicios.addLast(linkedListtServicios.getIndex(i));
					}
					//Comparar si se a�ade al principio
					else if(((Servicio) servicios.first()).darHorario().getHoraInicio().compareTo(linkedListtServicios.getIndex(i).darHorario().getHoraInicio())>=0){
						servicios.addFirst(linkedListtServicios.getIndex(i));
					}
					//Encontrar en cual mitad se debe a�adir
					else{
						//Revisar en cual mitad se debe a�adir 
						if(servicios.getIndex(Math.round(servicios.size()/2)).darHorario().getHoraInicio().compareTo(linkedListtServicios.getIndex(i).darHorario().getHoraInicio())<0){
							//A�adir en la primera mitad
							for(int j = 1; j <= Math.round(servicios.size()/2); j++){
								if(servicios.getIndex(j).darHorario().getHoraInicio().compareTo(linkedListtServicios.getIndex(i).darHorario().getHoraInicio())>=0){
									servicios.addIndex(j-1, linkedListtServicios.getIndex(i));
									break;
								}
							}
						}
						else{
							//A�adir en la segunda mitad
							for (int j = Math.round(servicios.size()/2); j <=servicios.size(); j++){
								if(servicios.getIndex(j).darHorario().getHoraInicio().compareTo(linkedListtServicios.getIndex(i).darHorario().getHoraInicio())>=0){
									servicios.addIndex(j-1, linkedListtServicios.getIndex(i));
								}
							}
						}
					}
				}
			}
		}

		//2.1
		for(int i = 1; i <= servicios.size(); i++){
			if(!(cont>DistMaxima)){
				resp.push(servicios.getIndex(i));
				cont=cont+servicios.getIndex(i).getTripMiles();
				distanciaTotal=distanciaTotal+servicios.getIndex(i).getTripMiles();
				tiempoTotal=tiempoTotal+Integer.parseInt(servicios.getIndex(i).getTripSeconds());
				valorTotal=valorTotal+servicios.getIndex(i).darTotal();
			}
			else{
				limSuper=true;
				distanciaTotal=distanciaTotal+servicios.getIndex(i).getTripMiles();
				tiempoTotal=tiempoTotal+Integer.parseInt(servicios.getIndex(i).getTripSeconds());
				valorTotal=valorTotal+servicios.getIndex(i).darTotal();
			}
		}

		if(limSuper){
			while(!resp.isEmpty()){
				resp.pop();
			}
			Servicio anadir = new Servicio(taxiId, null, null, null, String.valueOf(tiempoTotal), distanciaTotal, 0, 0, null, null, null, 0, 0, 0, 0, null);
			resp.push(anadir);
		}
		return resp;
	}

	private void resetStats (String  taxiId ,	String  tripId ,	String  tripStartTimestamp ,	String  tripEndTimestamp ,	int  tripSeconds ,	double  tripMiles ,	int  pickUpCensusTrack ,	int  dropOffCensusTrack ,	int  pickUpCommunityArea, int dropOffCommunityArea ,	String  company ,	double  pickUpLatitude ,	double  pickUpLongitude ,	double  dropOffLatitude ,	double  dropOffLongitude ,	String  paymentType ,	double  fare ,	double  tips ,	double  tolls ,	double  extras ,	double  tripTotal ){
		taxiId = null;
		tripId = null;
		tripStartTimestamp = null;
		tripEndTimestamp = null;
		tripSeconds = -1;
		tripMiles = -1;
		pickUpCensusTrack = -1;
		dropOffCensusTrack = -1;
		pickUpCommunityArea = -1;
		dropOffCommunityArea = -1;
		company = null;
		pickUpLatitude = -1;
		pickUpLongitude = -1;
		dropOffLatitude = -1;
		dropOffLongitude = -1;
		paymentType = null;
		fare = -1;
		tips = -1;
		tolls = -1;
		extras = -1;
		tripTotal = -1;
		FinancialData finData=null;
	}


	/**private String fixDateFormat(String date){
		///2013-01-25T18:15:00.000
		StringBuilder cons;
		String resp;
		String temp1;
		String temp2;
		cons=date;
	}
	/*/

}
