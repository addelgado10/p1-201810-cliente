package model.vo;

public class ServicioResumen implements Comparable<ServicioResumen> {

	private String horaInicial;
	private	String horaFinal;
	private double distancia;
	private int duracion;
	private double valorTotal;
	
	public ServicioResumen(){
		horaFinal=null;
		horaInicial=null;
		distancia=-1;
		duracion=-1;
		valorTotal=-1;
	}
	public String getHoraInicial() { return horaInicial;}
	public String getHoraFinal() { return horaFinal;}
	public double getDistancia() { return distancia;}
	public int getDuracion() { return duracion;}
	public double getValorTotal() { return valorTotal;}

	public void setHoraInicial (String horaInicial ){ this.horaInicial = horaInicial;}
	public void setHoraFinal (String horaFinal ){ this.horaFinal = horaFinal;}
	public void setDistancia (double distancia ){ this.distancia = distancia;}
	public void setDuracion (int duracion ){ this.duracion = duracion;}
	public void setValorTotal (double valorTotal ){ this.valorTotal = valorTotal;}
	@Override
	public int compareTo(ServicioResumen arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
