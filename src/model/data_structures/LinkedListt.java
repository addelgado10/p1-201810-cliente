package model.data_structures;

import java.util.Iterator;

public class LinkedListt<E> implements LinkedList{

	
	private class Node
	{
		public E item;
		public Node next;
	}
	
	private Node cabeza;
	
	
	private int tama�o;
	
	public LinkedListt()
	{
		cabeza = new Node();
		tama�o = 0;
	}
	
	public int size() {
		return tama�o;
	}

	 
	public boolean isEmpty() {
		return tama�o==0;
	}

	 
	public Comparable first() {
		return (Comparable) cabeza.item;
	}

	public void addFirst(Comparable element) {
		if (tama�o>0)
		{
			Node temp = new Node();
			temp = cabeza;
			cabeza = new Node();
			cabeza.item = (E) element;	
			cabeza.next = temp;
		}
		else
		{
			cabeza = new Node();
			cabeza.item = (E) element;
		}
		tama�o++;
	}
	 
	public void removeFirst() {
		if (!isEmpty())
		{
			cabeza = cabeza.next;
		}
		tama�o--;		
	}
	
	public void addIndex(int pos,Comparable element)
	{
		Node actual = cabeza;
		for(int i = 0;i < pos-1;i++)
		{
			actual = actual.next;
		}
		
		Node temp = new Node();
		temp.next = actual.next;
		actual.next = temp;
		temp.item = (E) element;
		tama�o++;
	}

	public void removeIndex(int pos)
	{
		if(pos<tama�o)
		{
			Node actual = new Node();
			Node anterior = new Node();
			if(pos ==0)
			{
				removeFirst();
			}
			else{
				actual = cabeza;
				for(int i = 0;i < pos-1;i++)
				{
					anterior = actual;
					actual = actual.next;
				}
				anterior.next = actual.next;
			}
		}
		tama�o--;
	}
	
	public void addLast(Comparable element) {
		// TODO Auto-generated method stub
		if(tama�o==0)
		{
			cabeza= new Node();
			cabeza.item = (E) element;
			tama�o++;
		}
		else
		{
			Node actual = cabeza;
			while(actual.next!=null)
			{
				actual = actual.next;
			}
			
			Node temp = new Node();
			temp.next = null;
			actual.next = temp;
			temp.item = (E) element;
			tama�o++;
		}

	}
	 
	public E getIndex(int pos)
	{
		if(pos<tama�o)
		{
			Node actual = cabeza;
			for(int i = 0;i < pos-1;i++)
			{
				actual = actual.next;
			}
			return actual.item;
		}
		else
			return null;
	}

}
