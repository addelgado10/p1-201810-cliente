package Test;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.LinkedListt;
import model.vo.Taxi;

public class LinkedListTest {

	private LinkedListt prueba;
	
	@Test
	public void testIsEmpty()
	{
		prueba = new LinkedListt<>();
		assertEquals( "La lista deberia estar vacia", true, prueba.isEmpty() );
		Taxi nuevo = new Taxi("123", "papitas");
		prueba.addFirst(nuevo);
		assertEquals( "La lista no deberia estar vacia", false, prueba.isEmpty() );
		prueba.removeFirst();
		assertEquals( "La lista deberia estar vacia", true, prueba.isEmpty() );
	}
	
	@Test
	public void testAddFirst()
	{
		prueba = new LinkedListt<>();
		Taxi nuevo = new Taxi("123", "papitas");
		prueba.addFirst(nuevo);
		assertEquals( "La lista no deberia estar vacia", false, prueba.isEmpty() );
		assertEquals( "El objeto no guardo bien", "123",nuevo.getTaxiId() );
		Taxi nuevo2 = new Taxi("425", "alans");
		prueba.addFirst(nuevo2);
		Taxi temp = (Taxi) prueba.first();
		assertEquals( "El objeto no guardo bien", "425",temp.getTaxiId() );
	}
	

	@Test
	public void testSize()
	{
		int contador = 0;
		prueba = new LinkedListt<>();
		Taxi nuevo = new Taxi("123", "papitas");
		assertEquals( "La lista debria ser de tama�o 0", 0, prueba.size() );
		prueba.addFirst(nuevo);
		assertEquals( "La lista debria ser de tama�o 1", 1,prueba.size() );
		Taxi nuevo1 = new Taxi("789", "papitas");
		Taxi nuevo2 = new Taxi("456", "alans");
		prueba.addFirst(nuevo1);
		prueba.addFirst(nuevo2);
		assertEquals( "La lista debria ser de tama�o 3", 3,prueba.size() );
		prueba.removeFirst();
		assertEquals( "La lista debria ser de tama�o 2", 2,prueba.size() );
	}
}
