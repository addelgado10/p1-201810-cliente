package Test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.data_structures.Queue;
import model.vo.Taxi;

public class QueueTest {
	
	private Queue prueba;
	
	@Test
	public void queueTest()
	{
		//Crea una cola vacia.
		prueba = new Queue();
		
		//Prueba si la cola esta vacia.
		assertEquals( "La cola deberia estar vacia", true, prueba.isEmpty() );
		
		//Agrega un taxi a la cola y prueba si esta vacia.
		Taxi nuevo = new Taxi("123", "papitas");
		prueba.enqueue(nuevo);
		assertEquals( "La cola no deberia estar vacia", false, prueba.isEmpty() );
		
		//Probar el tama�o de la cola al agregar un elemento.
		nuevo = new Taxi("456", "alans");
		prueba.enqueue(nuevo);
		assertEquals( "La cola deberia tener 2 elementos", 2, prueba.size() );
		
		//Prueba de orden de la cola.
		Taxi temp = (Taxi) prueba.dequeue();
		assertEquals( "El id del taxi es incorrecto", "123", temp.getTaxiId() );
		
		//Probar el tama�o de la cola al remover un elemento.
		assertEquals( "La cola deberia tener 1 elemento", 1, prueba.size() );
		
		//Prueba si la cola esta vacia y que el ultimo elmento corresponda.
		Taxi temp2 = (Taxi) prueba.dequeue();
		assertEquals( "La cola deberia tener 0 elemento", 0, prueba.size() );
		assertEquals( "La cola deberia estar vacia", true, prueba.isEmpty() );
		assertEquals( "La cola deberia estar vacia", "alans", temp2.getCompany());
	}

}
