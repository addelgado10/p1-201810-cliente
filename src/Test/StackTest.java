package Test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


import model.data_structures.Stack;
import model.vo.Taxi;

public class StackTest {
	
	private Stack prueba;
	
	@Test
	public void queueTest()
	{
		//Crea una pila vacia.
		prueba = new Stack();
		
		//Prueba si la pila esta vacia.
		assertEquals( "La pila deberia estar vacia", true, prueba.isEmpty() );
		
		//Agrega un taxi a la pila y prueba si esta vacia.
		Taxi nuevo = new Taxi("123", "papitas");
		prueba.push(nuevo);
		assertEquals( "La pila no deberia estar vacia", false, prueba.isEmpty() );
		
		//Probar el tama�o de la pila al agregar un elemento.
		nuevo = new Taxi("456", "alans");
		prueba.push(nuevo);
		assertEquals( "La pila deberia tener 2 elementos", 2, prueba.size() );
		
		//Prueba de orden de la pila.
		Taxi temp = (Taxi) prueba.pop();
		assertEquals( "El id del taxi es incorrecto", "456", temp.getTaxiId() );
		
		//Probar el tama�o de la pila al remover un elemento.
		assertEquals( "La pila deberia tener 1 elemento", 1, prueba.size() );
		
		//Prueba si la pila esta vacia y que el ultimo elmento corresponda.
		Taxi temp2 = (Taxi) prueba.pop();
		assertEquals( "La pila deberia tener 0 elemento", 0, prueba.size() );
		assertEquals( "La pila deberia estar vacia", true, prueba.isEmpty() );
		assertEquals( "La pila deberia estar vacia", "papitas", temp2.getCompany());
	}
}
